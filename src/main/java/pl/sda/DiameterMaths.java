package pl.sda;
public class DiameterMaths {
    public static double diameterToPerimeter(float diameter ) {
        double perimeter;
        perimeter = 3.14 * diameter;
        return perimeter;
    }
}
